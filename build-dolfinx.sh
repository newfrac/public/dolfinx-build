export UFL_GIT_COMMIT=main
export BASIX_GIT_COMMIT=main
export FFCX_GIT_COMMIT=main
export DOLFINX_GIT_COMMIT=main
export FLAGS="-O2"
#export PYBIND11_DIR=${PREFIX}


cd ${SRC_DIR} && \
  for repo in ufl ffcx dolfinx basix
    do
        if cd $repo; then git pull && cd ../; else git clone https://github.com/FEniCS/$repo; fi
    done


#-DCMAKE_BUILD_TYPE="Release" -DCMAKE_CXX_FLAGS_RELEASE="${FLAGS}" \
cd ${SRC_DIR}/basix && \
   cmake -DCMAKE_BUILD_TYPE=Release \
   -DCMAKE_INSTALL_PREFIX=${FENICS_DIR} \
   -B build-dir -S . && \
   cmake --build build-dir -- -j8 && \
   cmake --install build-dir && \
   CXXFLAGS="${FLAGS}" python3 -m pip install ./python

cd ${SRC_DIR}/ufl && \
    CXXFLAGS="${FLAGS}" python3 -m pip install --ignore-installed --no-dependencies .

cd ${SRC_DIR}/ffcx && \
    CXXFLAGS="${FLAGS}" python3 -m pip install --ignore-installed --no-dependencies .

cd ${SRC_DIR}/dolfinx && \
   mkdir -p build && \
   cd build && \
   cmake -DDOLFINX_SKIP_BUILD_TESTS=True \
     -DCMAKE_BUILD_TYPE=Release \
     -DCMAKE_INSTALL_PREFIX=${FENICS_DIR} \
     -DDOLFINX_ENABLE_PARMETIS=FALSE \
     -DADIOS2_DIR=${PREFIX} \
     ../cpp && \
   make -j16 all &&
   make install && \
   cd ../python && \
   CXXFLAGS="${FLAGS}" python3 -m pip install --ignore-installed --no-dependencies .
