#upgrade pip
pip3 install --upgrade pip

# LLVM_CONFIG required on aarch64, should be removed long-term.
pip3 install mpi4py numba && \
    pip3 install cffi cppimport flake8 isort pybind11==${PYBIND11_VERSION} pytest pytest-xdist sphinx sphinx_rtd_theme wheel

# Upgrade numpy via pip. Exclude binaries to avoid conflicts with libblas
# (See issue #126 and #1305)
pip3 install --no-binary="numpy" numpy==${NUMPY_VERSION} --upgrade

pip3 install cython
