export PETSC_SLEPC_OPTFLAGS="-O2"
export PETSC_SLEPC_MAKE_NP=24
export PETSC_SLEPC_DEBUGGING=0

cd ${SRC_DIR} && \
    wget -nc --quiet http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-lite-${PETSC_VERSION}.tar.gz -O petsc-${PETSC_VERSION}.tar.gz
    mkdir -p ${PETSC_DIR} && \
    tar -xf petsc-${PETSC_VERSION}.tar.gz -C ${PETSC_DIR} --strip-components 1 && \
    cd ${PETSC_DIR} && \
    python3 ./configure \
    --COPTFLAGS="${PETSC_SLEPC_OPTFLAGS}" \
    --CXXOPTFLAGS="${PETSC_SLEPC_OPTFLAGS}" \
    --FOPTFLAGS="${PETSC_SLEPC_OPTFLAGS}" \
    --with-make-np=${PETSC_SLEPC_MAKE_NP} \
    --with-64-bit-indices=no \
    --with-debugging=${PETSC_SLEPC_DEBUGGING} \
    --with-fortran-bindings=no \
    --with-shared-libraries \
    --download-ptscotch \
    --download-hypre \
    --download-mumps \
    --download-zlib=1 \
    --with-hdf5=1 \
    --download-hdf5=1 \
    --download-spai \
    --download-vtk \
    --download-scalapack \
    --download-suitesparse \
    --download-superlu_dist \
    --download-cmake \
    --with-scalar-type=real \
    --download-metis \
    --download-parmetis  && \
    make ${MAKEFLAGS} all

    # Install petsc4py
    cd ${PETSC_DIR}/src/binding/petsc4py && \
    pip3 install --no-cache-dir --prefix=${PREFIX} .

# Install SLEPc
cd ${SRC_DIR} && \
    rm -rf slepc-${SLEPC_VERSION}.tar.gz && \
    wget -nc https://slepc.upv.es/download/distrib/slepc-${SLEPC_VERSION}.tar.gz -O slepc-${SLEPC_VERSION}.tar.gz && \
    mkdir -p ${SLEPC_DIR} && \
    tar -xf slepc-${SLEPC_VERSION}.tar.gz -C ${SLEPC_DIR} --strip-components 1 && \
    cd ${SLEPC_DIR} && \
    python3 ./configure && \
    make && \
    cd src/binding/slepc4py && \
    pip3 install --no-cache-dir --prefix=${PREFIX} .
