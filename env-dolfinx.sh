# set additional compiler vars
export GMSH_VERSION=4_9_3
export PETSC_VERSION=3.16.3
export SLEPC_VERSION=3.16.1
export PETSC4PY_VERSION=3.16.0
export SLEPC4PY_VERSION=3.16.0
export ADIOS2_VERSION=2.7.1
export PYVISTA_VERSION=0.33.2
export KAHIP_VERSION=3.12
export XTENSOR_VERSION=0.23.10
export XTL_VERSION=0.7.3
export BUILD_THREADS=2
export PYBIND11_VERSION=2.8.1
export NUMPY_VERSION=1.21.5

export OPENBLAS_NUM_THREADS=1
export GOTO_NUM_THREADS=1
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1
export BLIS_NUM_THREADS=1

export ROOT_DIR=${HOME}/dev
export PREFIX=${ROOT_DIR}/dolfinx
mkdir -p ${PREFIX}

if [ -f ${PREFIX}/bin/activate ]; then
   echo "Using existing environment ${PREFIX}."
else
   echo "Creating environment ${PREFIX}."
   python3 -m venv ${PREFIX}
fi

source ${PREFIX}/bin/activate

export PETSC_DIR=${ROOT_DIR}/petsc
export SLEPC_DIR=${ROOT_DIR}/slepc
export PETSC_ARCH=arch-linux-c-opt
export HDF5_ROOT=${PETSC_DIR}/${PETSC_ARCH}
export HDF5_DIR=${HDF5_ROOT}
export FENICS_DIR=${PREFIX}
export SRC_DIR=${ROOT_DIR}/src/
mkdir -p ${SRC_DIR}

export PATH=${PREFIX}/bin:${PATH}
#export LD_LIBRARY_PATH=${PREFIX}/lib:${PREFIX}/lib64:${LD_LIBRARY_PATH}
export C_INCLUDE_PATH=${PREFIX}/include:${C_INCLUDE_PATH}
export CPLUS_INCLUDE_PATH=${PREFIX}/include:${CPLUS_INCLUDE_PATH}
export PKG_CONFIG_PATH=${PREFIX}/lib/pkgconfig:${PREFIX}/lib64/pkgconfig:${PKG_CONFIG_PATH}

echo "Dolfinx will be installed in ${PREFIX}."
echo "PETSc  will be installed in ${PETSC_DIR}."
echo "Add to your ~/.bash_profile file the following line:"
echo "    source ${PWD}/env-dolfinx.sh"

export LDFLAGS="-L/opt/homebrew/opt/llvm/lib"
export CPPFLAGS="-I/opt/homebrew/opt/llvm/include"