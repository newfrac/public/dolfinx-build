# Install guide

## On ubuntu 20.04

Execute the following lines in the order

1. Set the relevant environment variables
    ```
    source env-dolfinx.sh
    ```
2. Install basic linux pkgs with apt-get
    ```
    ./build-linux-deps.sh
    ```
3. Install python packages with pip 
    ```
    ./install-pip-deps.sh
    ```
4. Build and install PETSc:
    ```
    ./build-petsc.sh
    ```
4. Build and install Basix, FFX, UFL, Dolfinx:
    ```
    ./build-dolfinx.sh
    
## On macosx