# Install xtl, xtensor
echo "----------- Building XTL -${XTL_VERSION} and XTENSOR ${XTENSOR_VERSION} -----------"
cd ${SRC_DIR} && \
    rm -rf xtl xtensor && \
    git clone -b ${XTL_VERSION} --single-branch https://github.com/xtensor-stack/xtl.git && \
    cd xtl && \
    cmake -G Ninja -DCMAKE_INSTALL_PREFIX=${PREFIX} . && \
    ninja install && \
    cd ../ && \
    git clone -b ${XTENSOR_VERSION} --single-branch https://github.com/xtensor-stack/xtensor.git && \
    cd xtensor && \
    cmake -G Ninja -DCMAKE_INSTALL_PREFIX=${PREFIX} . && \
    ninja install && \
    cd ../

# Install ADIOS2
#echo "----------- Building adios2-${ADIOS2_VERSION} -----------"
#cd ${SRC_DIR} &&\
#    rm -rf adios2-v${ADIOS2_VERSION}* && \
#    wget -nc https://github.com/ornladios/ADIOS2/archive/v${ADIOS2_VERSION}.tar.gz -O adios2-v${ADIOS2_VERSION}.tar.gz && \
#    mkdir -p adios2-v${ADIOS2_VERSION} && \
#    tar -xf adios2-v${ADIOS2_VERSION}.tar.gz   -C adios2-v${ADIOS2_VERSION} --strip-components 1 && \
#    cd adios2-v${ADIOS2_VERSION} &&\
#    cmake -DCMAKE_INSTALL_PREFIX=${PREFIX} -DADIOS2_USE_HDF5=on -DADIOS2_USE_Fortran=off -DBUILD_TESTING=off -DADIOS2_BUILD_EXAMPLES=off -DADIOS2_USE_ZeroMQ=off -B #build-dir -S . && \
#    cmake --build build-dir && \
#    cmake --install build-dir && \
#    cd ..

# Install GMSH
echo "------------ Building gmsh-${GMSH_VERSION} -----------"
cd ${SRC_DIR} && \
    rm -rf gmsh && \
    git clone -b gmsh_${GMSH_VERSION} --single-branch https://gitlab.onelab.info/gmsh/gmsh.git && \
    cd gmsh && \\
    cmake -G Ninja -DCMAKE_INSTALL_PREFIX=${PREFIX} -DCMAKE_BUILD_TYPE=Release -DENABLE_BUILD_DYNAMIC=1  -DENABLE_OPENMP=1 -B build-dir -S . && \
    cmake --build build-dir && \
    cmake --install build-dir && \
    cd ..
