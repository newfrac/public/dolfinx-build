from contextlib import ExitStack

import numpy as np
import resource
import json
import sys
from dolfinx import BoxMesh, DirichletBC, Function, VectorFunctionSpace, cpp
from dolfinx.cpp.mesh import CellType
from dolfinx.fem import (
    apply_lifting,
    assemble_matrix,
    assemble_vector,
    locate_dofs_geometrical,
    set_bc,
)
from dolfinx.io import XDMFFile
from dolfinx.la import VectorSpaceBasis
import dolfinx
import dolfinx.common

from mpi4py import MPI
from petsc4py import PETSc
from pathlib import Path
from ufl import (
    Identity,
    TestFunction,
    TrialFunction,
    dx,
    grad,
    inner,
    sym,
    tr,
)
import argparse


def build_nullspace(V):
    """Function to build null space for 3D elasticity"""

    # Create list of vectors for null space
    index_map = V.dofmap.index_map
    nullspace_basis = [
        cpp.la.create_vector(index_map, V.dofmap.index_map_bs) for i in range(6)
    ]

    with ExitStack() as stack:
        vec_local = [stack.enter_context(x.localForm()) for x in nullspace_basis]
        basis = [np.asarray(x) for x in vec_local]

        # Dof indices for each subspace (x, y and z dofs)
        dofs = [V.sub(i).dofmap.list.array for i in range(3)]

        # Build translational null space basis
        for i in range(3):
            basis[i][dofs[i]] = 1.0

        # Build rotational null space basis
        x = V.tabulate_dof_coordinates()
        dofs_block = V.dofmap.list.array
        x0, x1, x2 = x[dofs_block, 0], x[dofs_block, 1], x[dofs_block, 2]
        basis[3][dofs[0]] = -x1
        basis[3][dofs[1]] = x0
        basis[4][dofs[0]] = x2
        basis[4][dofs[2]] = -x0
        basis[5][dofs[2]] = x1
        basis[5][dofs[1]] = -x2

    # Create vector space basis and orthogonalize
    basis = VectorSpaceBasis(nullspace_basis)
    basis.orthonormalize()

    _x = [basis[i] for i in range(6)]
    nsp = PETSc.NullSpace().create(vectors=_x)
    return nsp


def build_nullspace_2d(V):
    """Function to build null space for 3D elasticity"""

    # Create list of vectors for null space
    index_map = V.dofmap.index_map
    nullspace_basis = [
        cpp.la.create_vector(index_map, V.dofmap.index_map_bs) for i in range(3)
    ]

    with ExitStack() as stack:
        vec_local = [stack.enter_context(x.localForm()) for x in nullspace_basis]
        basis = [np.asarray(x) for x in vec_local]

        # Dof indices for each subspace (x, y and z dofs)
        dofs = [V.sub(i).dofmap.list.array for i in range(2)]

        # Build translational null space basis
        for i in range(2):
            basis[i][dofs[i]] = 1.0

        # Build rotational null space basis
        x = V.tabulate_dof_coordinates()
        dofs_block = V.dofmap.list.array
        x0, x1 = x[dofs_block, 0], x[dofs_block, 1]
        basis[2][dofs[0]] = -x1
        basis[2][dofs[1]] = x0

    # Create vector space basis and orthogonalize
    basis = VectorSpaceBasis(nullspace_basis)
    basis.orthonormalize()

    _x = [basis[i] for i in range(3)]
    nsp = PETSc.NullSpace().create(vectors=_x)
    return nsp

parser = argparse.ArgumentParser()
parser.add_argument(
    "--n_ref", default=1, type=np.int8, dest="n_ref", help="Number of divisions"
)
parser.add_argument(
    "--degree",
    default=1,
    type=np.int8,
    dest="degree",
    help="degree of finite element space",
)
parser.add_argument("--lambda", dest="lmbda", type=float, default=10, help="Lame coefficient lambda")
parser.add_argument("--pvd", dest="pvd", default=False, help="XDMF-output of function")
parser.add_argument("--xdmf", dest="xdmf", default=True, help="XDMF-output of function")
parser.add_argument(
    "--timings", dest="timings", default=True, help="List timings (Default false)"
)
parser.add_argument(
    "--ksp_view", dest="ksp_view", default=False, help="View PETSc progress"
)
parser.add_argument(
    "--monitor", dest="monitor", default=False, help="View PETSc progress"
)
parser.add_argument(
    "--prefix", default="output", dest="prefix", help="Prefix for file names"
)
parser.add_argument(
    "--outdir", default="output", dest="outdir", help="Directory for output file"
)
parser.add_argument(
    "--mesh_file", default=None, dest="mesh_file", help="Mesh file  for external mesh"
)
parser.add_argument(
    "--solver_type",
    dest="solver_type",
    default="all",
    help="Solver to use (gamg or mumps or all)",
)
args = parser.parse_args()
thismodule = sys.modules[__name__]
n_ref = timings = boomeramg = kspview = degree = hdf5 = xdmf = tetra = None

for key in vars(args):
    setattr(thismodule, key, getattr(args, key))

if MPI.COMM_WORLD.rank == 0:
    Path(outdir).mkdir(parents=True, exist_ok=True)

L = 1
W = 0.2
nx = 5 * n_ref
ny = 1 * n_ref
nz = 1 * n_ref


with dolfinx.common.Timer("~Elasticity: Meshing and BCs") as timer:

    if mesh_file:
        #mesh_file = "meshes/holed_plate-4-1-0.1-1.xdmf"
        with XDMFFile(MPI.COMM_WORLD, mesh_file, "r") as file:
            mesh = file.read_mesh()

        for i in range(n_ref):
            mesh.topology.create_entities(1)
            mesh = dolfinx.mesh.refine(mesh, redistribute=True)
    else:
        mesh = BoxMesh(
            MPI.COMM_WORLD,
            [np.array([0.0, 0.0, 0.0]), np.array([L, W, W])],
            [nx, ny, nz],
            CellType.tetrahedron,
            dolfinx.cpp.mesh.GhostMode.shared_facet,
         )
    
    # Loading due to centripetal acceleration (rho*omega^2*x_i)

    V = VectorFunctionSpace(mesh, ("Lagrange", 1))
    u0 = Function(V)
    with u0.vector.localForm() as bc_local:
        bc_local.set(0.0)

    bc = DirichletBC(u0, locate_dofs_geometrical(V, lambda x: np.isclose(x[0], 0.0)))
    mesh_bc_time = timer.elapsed()


with dolfinx.common.Timer("~Elasticity: Assemble LHS and RHS") as timer:
    mu = 1.
    lmbda = dolfinx.Constant(mesh,lmbda)
    f = dolfinx.Constant(mesh, 1.)
    u = TrialFunction(V)
    v = TestFunction(V)
    eps = lambda u: sym(grad(u))
    sigma = lambda eps: 2.0 * mu * sym(eps) + lmbda * tr(eps) * Identity(len(u))
    a = inner(sigma(eps(u)), grad(v)) * dx
    L = f * v[1] * dx
    A = assemble_matrix(a, [bc])
    A.assemble()
    b = assemble_vector(L)
    apply_lifting(b, [a], [[bc]])
    b.ghostUpdate(addv=PETSc.InsertMode.ADD, mode=PETSc.ScatterMode.REVERSE)
    set_bc(b, [bc])
    assembly_time = timer.elapsed()


def solve_gamg(A, b, u):
    opts = PETSc.Options()
    opts["ksp_type"] = "cg"
    opts["ksp_rtol"] = 1.0e-12
    opts["pc_type"] = "gamg"
    opts["mg_levels_ksp_type"] = "chebyshev"
    opts["mg_levels_pc_type"] = "jacobi"
    opts["mg_levels_esteig_ksp_type"] = "cg"
    opts["mg_levels_ksp_chebyshev_esteig_steps"] = 20
    if mesh.geometry.dim == 3:
        null_space = build_nullspace(u.function_space)
    else:
        null_space = build_nullspace_2d(u.function_space)
    A.setNearNullSpace(null_space)
    solver = PETSc.KSP().create(u.function_space.mesh.mpi_comm())
    solver.setFromOptions()
    solver.setOperators(A)
    if monitor:
        solver.setMonitor(
            lambda ksp, its, rnorm: print(
                "Iteration: {}, rel. residual: {}".format(its, rnorm)
            )
        )
    solver.solve(b, u.vector)
    it = solver.getIterationNumber()
    if ksp_view:
        solver.view()
    return it


def solve_mumps(A, b, u):
    opts = PETSc.Options()
    opts["ksp_type"] = "preonly"
    opts["pc_type"] = "lu"
    opts["pc_factor_mat_solver_type"]: "mumps"
    solver = PETSc.KSP().create(u.function_space.mesh.mpi_comm())
    solver.setFromOptions()
    solver.setOperators(A)
    if monitor:
        solver.setMonitor(
            lambda ksp, its, rnorm: print(
                "Iteration: {}, rel. residual: {}".format(its, rnorm)
            )
        )
    solver.solve(b, u.vector)
    if ksp_view:
        solver.view()
    return None


xdmf_saving_time = pvd_saving_time = 0
gamg_solver_time = mumps_solver_time = it = 0

if solver_type == "gamg" or solver_type == "all":
    with dolfinx.common.Timer("~Elasticity: Iterative Solver") as timer:
        u = Function(V)
        it = solve_gamg(A, b, u)
        gamg_solver_time = timer.elapsed()

if solver_type == "mumps" or solver_type == "all":
    with dolfinx.common.Timer("~Elasticity: Direct Solver") as timer:
        u = Function(V)
        solve_mumps(A, b, u)
        mumps_solver_time = timer.elapsed()

if xdmf  == True:
    with dolfinx.common.Timer("~Elasticity: Save XDMF") as timer:
        with XDMFFile(
            mesh.mpi_comm(),
            f"{outdir}/elasticity.xdmf",
            "w",
            encoding=XDMFFile.Encoding.HDF5,
        ) as file:
            file.write_mesh(mesh)
            file.write_function(u, 1)
        xdmf_saving_time = timer.elapsed()

    with dolfinx.common.Timer("~Elasticity: Save PVD") as timer:
        with dolfinx.io.VTKFile(
            MPI.COMM_WORLD, f"{outdir}/elasticity.pvd", "w"
        ) as file:
            file.write(mesh)
            file.write_function(u, 1.0)
        pvd_saving_time = timer.elapsed()


# Print max usage of summary
mem = sum(MPI.COMM_WORLD.allgather(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss))
num_dofs = V.dofmap.index_map.size_global * V.dofmap.index_map_bs


dolfinx.common.list_timings(MPI.COMM_WORLD, [dolfinx.common.TimingType.wall])
data = {
    "num_processors": MPI.COMM_WORLD.size,
    "solver_type": solver_type,
    "iterations": it,
    "memory": mem,
    "n_dofs": num_dofs,
    "gamg_solver_time": gamg_solver_time,
    "mumps_solver_time": mumps_solver_time,
    "assembly_time": assembly_time,
    "mesh_bc_time": mesh_bc_time,
    "xdmf_saving_time": xdmf_saving_time,
    "pvd_saving_time": pvd_saving_time,
}


if MPI.COMM_WORLD.rank == 0:
    print("-------")
    print("Summary")
    print("-------")

    for k, v in data.items():
        print(k, v)

    with open(
        f"{outdir}/{prefix}-{n_ref}-{MPI.COMM_WORLD.size}-{solver_type}.txt", "w"
    ) as file:
        file.write(json.dumps(data))